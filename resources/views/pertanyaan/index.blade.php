@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tabel Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('Success'))
            <div class="alert alert-success">
                {{ session('Success') }}
            </div>
            @endif
            <a class="btn btn-info mb-3" href="/pertanyaan/create">Create New Pertanyaan</a>
          <table class="table table-bordered">
            <thead>                  
                <tr>
                    <th style="width: 10px">No.</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
             @forelse ($pertanyaan as $key => $pertanyaan1)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $pertanyaan1->judul }} </td>
                    <td> {{ $pertanyaan1->isi }} </td>
                    <td style="display: flex;">
                    <a href="/pertanyaan/{{$pertanyaan1->id}}" class="btn btn-info btn-sm ml-1 mr-1">Show</a>
                    <a href="/pertanyaan/{{$pertanyaan1->id}}/edit" class="btn btn-success btn-sm ml-1 mr-1">Edit</a>
                    <form action="/pertanyaan/{{$pertanyaan1->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                </tr>
             @empty
                 <tr>
                     <td colspan="4" align="center">No Post</td>
                 </tr>
             @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> --}}
      </div>
</div>
    
@endsection