<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJudulToPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->string('judul')->after('id');
            $table->longText('isi')->after('judul');
            $table->date('tanggal_dibuat')->after('isi');
            $table->date('tanggal_diperbaharui')->after('tanggal_dibuat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropColumn(['tanggal_diperbaharui']);
            $table->dropColumn(['tanggal_dibuat']);
            $table->dropColumn(['isi']);
            $table->dropColumn(['judul']);
        });
    }
}
